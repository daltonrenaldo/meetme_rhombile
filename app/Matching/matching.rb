# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class Matching
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with Matching.
  enable :sync

  #add model specifc code here
end
