require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/application_helper.rb'

class MatchingController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  
  # GET /Matching
  def index
    require_login
    @matchings = current_user.matches

    render :back => '/app'
  end
  
  def saved_match
    require_login
    
    @matchs = Matching.find(:all)
    @matchings = []
    # Adding User info to Match
    @matchs.each do |match|
      if current_user.object == match.user_id
        @matchings.push Profile.retrieve(match.match_id)
#        match.bio = Biography.retrieve(match.match_id)
      else
        @matchings.push Profile.retrieve(match.user_id)
#        match.bio = Biography.retrieve(match.user_id)
      end
    end
#    @result = User.retrieve(1)
    render :action => :saved, :back => '/app'
  end

  # GET /Matching/{1}
  def show
    @matching = Matching.find(@params['id'])
    if @matching
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Matching/new
  def new
    @matching = Matching.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Matching/{1}/edit
  def edit
    @matching = Matching.find(@params['id'])
    if @matching
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Matching/create
  def create
    @matching = Matching.create(@params['matching'])
    redirect :action => :index
  end

  # POST /Matching/{1}/update
  def update
    @matching = Matching.find(@params['id'])
    @matching.update_attributes(@params['matching']) if @matching
    redirect :action => :index
  end

  # POST /Matching/{1}/delete
  def delete
    @matching = Matching.find(@params['id'])
    @matching.destroy if @matching
    redirect :action => :index  
  end
end
