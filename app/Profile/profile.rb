class Profile
  include Rhom::PropertyBag
  
  # These methods below uses asynchttp to get values
  # that are not being synced with rhoconnect from MeetMe's API
  
  def self.retrieve(user_id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/profiles/#{user_id}.json"
    )
    @get_result = self.new result["body"] # This creates a user in memory based on the returned JSON
  end
  
#  def self.create(args)
#    result = Rho::AsyncHttp.post(
#      :url => "#{Rho::RhoConfig.api_path}/profile/create.json",
#      :body => args
#    )
#    if result['http_error']
#      result
#    else
#      true
#    end
#  end
  
end
