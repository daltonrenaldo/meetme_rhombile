require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/application_helper'

class ProfileController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  
  def index
    @user = current_user
    render :back => Rho::RhoConfig.start_path
  end
  
  def show
    @profile = Profile.retrieve(strip_braces(@params['id']))
    render :back => Rho::RhoConfig.start_path
  end
  
  def create
    
  end
  
  def camera
    settings = { :camera_type => "main"}
    Camera::take_picture(url_for(:action => :camera_callback), settings)
    Alert.show_popup Camera::get_camera_info("main")
  end
  
  def camera_callback
  end
  
  def edit
    render :action => :edit_menu
  end
  
  def interests_update
   @interests = @params['interests']
   @interests = @interests.join(',')
    results = Rho::AsyncHttp.post(
          :url => "#{Rho::RhoConfig.api_path}/profile/#{current_user.object}/update_interests.json",
          :body => "interests=#{@interests}",
          :callback => url_for(:action => :update_interests_callback)
        )
    @response['headers']['Wait-Page'] = 'true'
    render :action => :wait
#    render :action => :test, :query => {:result => @result }
  end
  
  def update_interests_callback
    if @params['status'] != 'error'
      @msg = "Account Successfully Created."
      WebView.navigate url_for( :action =>  :index, :query=> {:msg => @msg})
    else
      @msg = "Error"
     WebView.navigate url_for( :action =>  :index, :query=> {:msg => @msg})
    end
  end
end
