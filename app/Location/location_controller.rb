require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/application_helper'

class LocationController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper

  # GET /Location
  def index
    require_login
    @locations = Location.find(:all)
    render :back => '/app'
  end

  # GET /Location/{1}
  def show
    require_login
    @location = Location.find(@params['id'])
    if @location
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Location/new
  def new
    require_login
    @location = Location.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Location/{1}/edit
  def edit
    require_login
    @location = Location.find(@params['id'])
    if @location
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Location/create
  def create
    require_login
    @location = Location.create(@params['location'])
    redirect :action => :index
  end

#  # POST /Location/{1}/update
#  def update
#    @location = Location.find(@params['id'])
#    @location.update_attributes(@params['location']) if @location
#    redirect :action => :index
#  end

 #TODO: Need to be a class function (self) that is called every time we sync  
  def update
    require_login
    @location = Location.find(:first)
    if GeoLocation.haversine_distance(@location.latitude.to_f, @location.longitude.to_f, GeoLocation.latitude, GeoLocation.longitude) > 1
      @location.latitude = GeoLocation.latitude
      @location.longitude = GeoLocation.longitude
      @location.save
      GeoLocation.turnoff # not sure if that turnon is required after
    end
    redirect :action => :index
  end

  # POST /Location/{1}/delete
  def delete
    require_login
    @location = Location.find(@params['id'])
    @location.destroy if @location
    redirect :action => :index  
  end
end
