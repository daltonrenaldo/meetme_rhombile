# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class Location
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with Location.
  enable :sync

  #add model specifc code here
  
  def self.renew(u_id)
    result = Rho::AsyncHttp.post(
       :url => "#{Rho::RhoConfig.api_path}/profile/#{u_id}/update_location.json?latitude=#{GeoLocation.latitude.to_s}&longitude=#{GeoLocation.longitude.to_s}"
   )
  end
end
