# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class User
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with User.
  enable :sync

  #add model specifc code here
  def biography
    Biography.find(:first, :conditions => {:user_id => self.object})
  end
  
  def interests(id_only = false)
    if id_only
      interests = Interest.find(:all)
      ids = []
      interests.each do |interest|
        ids.push interest.object
      end
      ids
    else
      Interest.find(:all)
    end
  end
  
  def location
    Location.find(:first)
  end
  
  def matches
    @matches = []
    results = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/profile/#{self.object}/match.json"
    )
    results["body"].each do |result|
      @matches.push( User.new( result) ) # This creates a user in memory based on the returned JSON
    end
    @matches
  end
  
  def match_status(match_id)
    if match = Matching.find(:first, :conditions => {:user_id => self.object, :match_id => match_id })
      case match.status
      when "pending"
        return ["user_pending", match.object] # meaning the user initiated the request and the request is pending
      when "blocked"
        return ["user_blocked", match.object] # meaning the user did the blocking
      else
        return ["accepted", match.object]
      end
    elsif match = Matching.find(:first, :conditions => {:user_id => match_id, :match_id => self.object })
      case match.status
        when "pending"
          return ["match_pending", match.object] # meaning the match initiated the request and the request is pending
        when "blocked"
          return ["match_blocked", match.object] # meaning the match did the blocking
        else
          return ["accepted", match.object] #should be accepted
      end
    else
      [false, false]
    end
  end
  
  
  # These methods below uses asynchttp to get values
  # that are not being synced with rhoconnect from MeetMe's API
  
  def self.retrieve(user_id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/users/#{user_id}.json"
    )
    @get_result = self.new result["body"] # This creates a user in memory based on the returned JSON
  end
  
  def self.username(id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/users/#{id}/username.json"
    )
    @get_result = result["body"]
  end
  
  def self.fullname(id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/users/#{id}/name.json"
    )
    @get_result = result["body"]
  end
  
  def self.email(id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/users/#{id}/email.json"
    )
    @get_result = result["body"]
  end
  
  def self.photo(id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/users/#{id}/photo.json"
    )
    @get_result = result["body"]
  end
end
