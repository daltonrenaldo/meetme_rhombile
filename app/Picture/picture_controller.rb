require 'rho/rhocontroller'
require 'helpers/browser_helper'

class PictureController < Rho::RhoController
  include BrowserHelper

  # GET /Picture
  def index
    @pictures = Picture.find(:all)
    render :back => '/app'
  end

  # GET /Picture/{1}
  def show
    @picture = Picture.find(@params['id'])
    if @picture
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Image/new
  def new
    settings = { :camera_type => get_camera_type }
    Camera::take_picture(url_for( :action => :camera_callback), settings)  # take a picture
    ""               # render nothing
  end
  
  def edit
    Camera::choose_picture(url_for :action => :camera_callback)   # choose a picture
    ""
  end
  
  def camera_callback
    if @params['status'] == 'ok'
      image = Picture.new({'photo' => @params['image_uri']})
      image.save
    end
  
    WebView.navigate( url_for :action => :index )
    ""
  end
  
  def get_camera_type
     if Camera::get_camera_info("front")
       "front"
     else
       "main"
     end
  end

  # POST /Picture/create
  def create
    @picture = Picture.create(@params['picture'])
    redirect :action => :index
  end

  # POST /Picture/{1}/update
  def update
    @picture = Picture.find(@params['id'])
    @picture.update_attributes(@params['picture']) if @picture
    redirect :action => :index
  end

  # POST /Picture/{1}/delete
  def delete
    @picture = Picture.find(@params['id'])
    @picture.destroy if @picture
    redirect :action => :index  
  end
end
