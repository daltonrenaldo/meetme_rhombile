# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class Interest
  include Rhom::PropertyBag

  # This only syncs user's specific interests.
  enable :sync



  # Returns all interests from server
  def self.retrieve
    results = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/interests.json"
    )
    @results = []
    results['body'].each do |result|
      @results.push( self.new( result))
    end
    return @results
  end
end
