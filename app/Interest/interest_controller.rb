require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/application_helper'

class InterestController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  
  # GET /Interest
  def index
    @interests = Interest.find(:all)
    render :back => '/app'
  end

  # GET /Interest/{1}
  def show
    @interest = Interest.find(@params['id'])
    if @interest
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Interest/new
  def new
    @interest = Interest.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Interest/{1}/edit
  def edit
    @interests = Interest.retrieve
    if @interests
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Interest/create
  def create
    @interest = Interest.create(@params['interest'])
    redirect :action => :index
  end

  # POST /Interest/{1}/update
  def update
    @interest = Interest.find(@params['id'])
    @interest.update_attributes(@params['interest']) if @interest
    redirect :action => :index
  end

  # POST /Interest/{1}/delete
  def delete
    @interest = Interest.find(@params['id'])
    @interest.destroy if @interest
    redirect :action => :index  
  end
  
  def test
    @result = @params
    render :action => :test
  end
end
