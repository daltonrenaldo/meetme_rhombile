require 'rho'
require 'rho/rhocontroller'
require 'rho/rhoerror'
require 'helpers/browser_helper'
require 'helpers/application_helper'

class SettingsController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  
  def index
    @msg = @params['msg']
    render
  end

  def login
    @msg = @params['msg']
    render :action => :login
  end

  def login_callback
    errCode = @params['error_code'].to_i
    if errCode == 0
      # run sync if we were successful
      WebView.navigate Rho::RhoConfig.options_path
      SyncEngine.dosync
    else
      if errCode == Rho::RhoError::ERR_CUSTOMSYNCSERVER
        @msg = @params['error_message']
      end
        
      if !@msg || @msg.length == 0   
        @msg = Rho::RhoError.new(errCode).message
      end
      
      WebView.navigate ( url_for :action => :login, :query => {:msg => @msg} )
    end  
  end

  def do_login
    if @params['login'] and @params['password']
      begin
        SyncEngine.login(@params['login'], @params['password'], (url_for :action => :login_callback) )
        @response['headers']['Wait-Page'] = 'true'
        render :action => :wait
      rescue Rho::RhoError => e
        @msg = e.message
        render :action => :login
      end
    else
      @msg = Rho::RhoError.err_message(Rho::RhoError::ERR_UNATHORIZED) unless @msg && @msg.length > 0
      render :action => :login
    end
  end
  
  def logout
    $session = {}
    SyncEngine.logout
    @msg = "You have been logged out."
    render :action => :login
  end
  
  def signup
    render :back => url_for(:action => :login)
  end
  
  def do_signup
    args = "username=#{@params['username']}"
    args += "&email=#{@params['email']}"
    args += "&password=#{@params['password']}"
    args += "&name=#{@params['name']}"
    args += "&about=#{@params['about']}"
    args += "&birthday=#{@params['birthday']}"
    args += "&occupation=#{@params['occupation']}"
    args += "&photo=#{@params['photo']}"
    args += "&longitude=#{GeoLocation.longitude}"
    args += "&latitude=#{GeoLocation.latitude}"
    
    GeoLocation.turnoff
    
    @email = @params['email']
    @pass = @params['password']
    
    Rho::AsyncHttp.post(
      :url => "#{Rho::RhoConfig.api_path}/profile/create.json",
      :body => args,
      :callback => url_for(:action => :signup_callback)
    )
    
    @response['headers']['Wait-Page'] = 'true'
    render :action => :wait
  end
  
  def signup_callback
    if @params['status'] != 'error'
      @msg = "Account Successfully Created."
      WebView.navigate url_for :action => :login, :query => {:msg => @msg}
    else
      #TODO: get this @errors to actually be passed to the page 
      @errors = @params['body']
      WebView.navigate( url_for( :action => :signup, :query => {:errors => @errors}))
    end
  end
  
  def reset
    render :action => :reset
  end
  
  def do_reset
    Rhom::Rhom.database_full_reset
    SyncEngine.dosync
    @msg = "Database has been reset."
    redirect :action => :index, :query => {:msg => @msg}
  end
  
  def do_sync
    SyncEngine.dosync
    if current_user # if it has already sync the user down
      Location.renew(current_user.object)
      GeoLocation.turnoff
      
      match = current_user.matches
      match.each do |m|
        if m.distance_away > 0.2
          m.delete
        end
      end if match
      
      $session[:match_intro] = nil
        
      if !match.empty?
        for i in 0..(match.length - 1)
          
          num = [*0..(match.length - 1)].sample
          if !$session[:shown_matches].include? match[num].id
            $session[:shown_matches].push match[num].id
            $session[:match_intro] = match[num]
            $session[:just_synced] = true
            break
          end
        end
      end
    else
      SyncEngine.dosync # let's try one more time
    end
    
    @msg =  "Sync has been triggered."
    redirect :action => :index, :query => {:msg => @msg}
  end
  
  def sync_notify
        status = @params['status'] ? @params['status'] : ""
        
        # un-comment to show a debug status pop-up
        #Alert.show_status( "Status", "#{@params['source_name']} : #{status}", Rho::RhoMessages.get_message('hide'))
        
    if status == "in_progress"      
      # do nothing
    elsif status == "complete"
      WebView.navigate Rho::RhoConfig.start_path if @params['sync_type'] != 'bulk'
    elsif status == "error"
      
      if SyncEngine::logged_in <= 0
        WebView.navigate ( url_for :action => :login)
      end
      
      if @params['server_errors'] && @params['server_errors']['create-error']
        SyncEngine.on_sync_create_error( 
          @params['source_name'], @params['server_errors']['create-error'].keys, :delete )
      end

      if @params['server_errors'] && @params['server_errors']['update-error']
        SyncEngine.on_sync_update_error(
          @params['source_name'], @params['server_errors']['update-error'], :retry )
      end
      
      err_code = @params['error_code'].to_i
      rho_error = Rho::RhoError.new(err_code)
      
      @msg = @params['error_message'] if err_code == Rho::RhoError::ERR_CUSTOMSYNCSERVER
      @msg = rho_error.message unless @msg && @msg.length > 0   

      if rho_error.unknown_client?( @params['error_message'] )
        Rhom::Rhom.database_client_reset
        SyncEngine.dosync
      elsif err_code == Rho::RhoError::ERR_UNATHORIZED
        WebView.navigate( 
          url_for :action => :login, 
          :query => {:msg => "Server credentials are expired"} )                
      elsif err_code != Rho::RhoError::ERR_CUSTOMSYNCSERVER
        WebView.navigate( url_for :action => :err_sync, :query => { :msg => @msg } )
      end    
    end
  end  
  
#  def push_callback
#    "rho_push"
#  end
end