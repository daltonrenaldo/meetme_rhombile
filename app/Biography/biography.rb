# The model has already been created by the framework, and extends Rhom::RhomObject
# You can add more methods here
class Biography
  include Rhom::PropertyBag

  # Uncomment the following line to enable sync with Biography.
  enable :sync

  belongs_to :user_id, 'User'
  property :image_uri, :blob     # the image is a blob
  
  
  
  # These methods below uses asynchttp to get values
  # that are not being synced with rhoconnect from MeetMe's API
  
  def self.retrieve(user_id)
    result = Rho::AsyncHttp.get(
      :url => "#{Rho::RhoConfig.api_path}/biography/user.json?user_id=#{user_id}"
    )
    @get_result = self.new result["body"] # This creates a user in memory based on the returned JSON
  end
end
