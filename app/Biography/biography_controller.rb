require 'rho/rhocontroller'
require 'helpers/browser_helper'
require 'helpers/application_helper'

class BiographyController < Rho::RhoController
  include BrowserHelper
  include ApplicationHelper
  
  # GET /Biography
  def index
    @biographies = Biography.find(:all)
    render :back => '/app'
  end

  # GET /Biography/{1}
  def show
    @biography = Biography.find(@params['id'])
    if @biography
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Biography/new
  def new
    @biography = Biography.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Biography/{1}/edit
  def edit
    @biography = Biography.find(@params['id'])
    if @biography
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Biography/create
  def create
    @biography = Biography.create(@params['biography'])
    redirect :action => :index
  end

  # POST /Biography/{1}/update
  def update
    @biography = Biography.find(@params['id'])
    @biography.update_attributes(@params['biography']) if @biography
    redirect :controller => :Profile, :action => :index
  end

  # POST /Biography/{1}/delete
  def delete
    @biography = Biography.find(@params['id'])
    @biography.destroy if @biography
    redirect :action => :index  
  end
  
  def edit_pic
    render :action => :edit_pic
  end
  
  def new_picture
     settings = { :camera_type => get_camera_type }
     Camera::take_picture(url_for( :action => :camera_callback, :id => @params['id']), settings)  # take a picture
     ""               # render nothing
   end
   
   def edit_picture
     Camera::choose_picture(url_for :action => :camera_callback, :id => @params['id'])   # choose a picture
     ""
   end
   
   def camera_callback
     success = false
     if @params['status'] == 'ok'
       
       @biography = Biography.find(@params['id'])
       # @biography.image_uri = @params['image_uri'] if @biography
#       @biography.save
        @biography.update_attributes({'image_uri' => @params['image_uri']}) if @biography
#       image = Biography.update({'photo' => @params['image_uri']})
#       image.save
       WebView.navigate( url_for( :controller => :Profile, :action => :index ))
     else
       WebView.navigate( url_for( :action => :edit_pic, :id => @params['id'] ))
     end
     
     ""
   end
   
   def get_camera_type
      if Camera::get_camera_info("front")
        "front"
      else
        "main"
      end
   end
end
