require 'rho/rhocontroller'
require 'helpers/application_helper'
require 'helpers/browser_helper'

class MessageController < Rho::RhoController
  include ApplicationHelper
  include BrowserHelper

  # GET /Message
  def index
    @messages = Message.find(:all, :conditions =>{'recipient_id'=> current_user.object, 'recipient_deleted' => ''}, :order => 'created_at', :orderdir => 'DESC')
    @page = "Inbox"
    render :back => '/app', :action => :index
  end
  
  # GET sent /Message
  def sent
    @messages = Message.find(:all, :conditions =>{'user_id'=> current_user.object, 'user_deleted' => ''}, :order => 'created_at', :orderdir => 'DESC')
    @page = "Sent"
    render :back => '/app', :action => :index
  end

  # GET /Message/{1}
  def show
    @message = Message.find(@params['id'])
    if @message
      render :action => :show, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # GET /Message/new
  def new
    @message = Message.new
    render :action => :new, :back => url_for(:action => :index)
  end

  # GET /Message/{1}/edit
  def edit
    @message = Message.find(@params['id'])
    if @message
      render :action => :edit, :back => url_for(:action => :index)
    else
      redirect :action => :index
    end
  end

  # POST /Message/create
  def create
    @message = Message.create(@params['message'])
    redirect :action => :index
  end

  # POST /Message/{1}/update
  def update
    @message = Message.find(@params['id'])
    @message.update_attributes(@params['message']) if @message
    redirect :action => :index
  end

  # POST /Message/{1}/delete
  def delete
    @message = Message.find(@params['id'])
    @message.update_attributes({'recipient_deleted' => true}) if @message
    redirect :action => :index  
  end
end
