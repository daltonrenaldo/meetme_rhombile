# MeetMe Rhomobile

Author: Renaldo Pierre-Louis
Date: October 15, 2012

MeetMe is a social mobile application designed to facilate the encounter of people with similar interests. MeetMe aims to revolutionize the social media world, especially the dating aspect of it. 


### Getting Started

1. Clone the repo

2. Since MeetMe Rhodes (cross platform framework by Rhomobile), download and install Rhomobile Suite (instructions can be found at their websites)

3. Download necessary SDK (Android, iOS), also NDK if Android.
	
3. Run the Rho simulator. If on linux, you will have to compile using the SDKs; No simulator for linux
	$ rake run:iphone:rhosimulator  # or rake run:android:rhosimulator

	You can also not use simulator with:
	$ rake run:iphone
	
4. Set syncserver in rhoconfig.txt to 'http://rhoserviced4e2f5e6.rhoconnect.com/application' 
5. Create an account oh http://meet-me-app.herokuapp.com  and have fun.